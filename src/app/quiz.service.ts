import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Quiz } from './quiz';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  quizzes: Quiz[] = [
    {
      question: "What was the name of the hero in the 80s animated video game Dragons Lair?",
      category: "Entertainment Video Games",
      answer: [
        { option: 'Arthur', correct: false},
        { option: 'Sir Toby Belch', correct: false},
        { option: 'Guy of Gisbourne', correct: false},
        { option: 'Dirk the Daring', correct: true},
      ]
    },
    {
      question: "What is the scientific name for modern day humans?",
      category: "Animals",
      answer: [
        { option: 'Homo Ergaster', correct: false},
        { option: 'Homo Erectus', correct: false},
        { option: 'Homo Neanderthalensis', correct: false},
        { option: 'Homo Sapiens', correct: true},
      ]
    },
    {
      question: "What is Ron Weasleys middle name?",
      category: "Entertainment Books",
      answer: [
        { option: 'Arthur', correct: false},
        { option: 'John', correct: false},
        { option: 'Dominic', correct: false},
        { option: 'Bilius', correct: true},
      ]
    },
    {
      question: "Who is the creator of the comic series The Walking Dead?",
      category: "Entertainment Comics",
      answer: [
        { option: 'Stan Lee', correct: false},
        { option: 'Malcolm Wheeler-Nicholson', correct: false},
        { option: 'Robert Crumb', correct: false},
        { option: 'Robert Kirkman', correct: true},
      ]
    },
    {
      question: "At the start of a standard game of the Monopoly if you throw a double six which square would you land on?",
      category: "Entertainment Board Games",
      answer: [
        { option: 'Water Works', correct: false},
        { option: 'Chance', correct: false},
        { option: 'Community Chest', correct: false},
        { option: 'Electric Company', correct: true},
      ]
    },{
      question: "What is the capital of Jamaica?",
      category: "Geography",
      answer: [
        { option: 'Rio de Janeiro', correct: false},
        { option: 'Dar es Salaam', correct: false},
        { option: 'Kano', correct: false},
        { option: 'Kingston', correct: true},
      ]
    },{
      question: "When did Jamaica recieve its independence from England? ",
      category: "History",
      answer: [
        { option: '1492', correct: false},
        { option: '1963', correct: false},
        { option: '1987', correct: false},
        { option: '1962', correct: true},
      ]
    },{
      question: "Kangaroos keep food in their pouches next to their children.",
      category: "Animals",
      answer: [
        { option: 'All', correct: false},
        { option: 'None of These', correct: false},
        { option: 'True', correct: false},
        { option: 'False', correct: true},
      ]
    },{
      question: "In 2013 how much money was lost by Nigerian scams?",
      category: "General Knowledge",
      answer: [
        { option: '95 Million', correct: false},
        { option: '956 Million', correct: false},
        { option: '2.7 Billion', correct: false},
        { option: '12.7 Billion', correct: true},
      ]
    },{
      question: "How old was Lyndon B. Johnson when he assumed the role of President of the United States?",
      category: "History",
      answer: [
        { option: '50', correct: false},
        { option: '60', correct: false},
        { option: '54', correct: false},
        { option: '55', correct: true},
      ]
    },{
      question: "In World of Warcraft lore who organized the creation of the Paladins?",
      category: "Entertainment Video Games",
      answer: [
        { option: 'Uther the Lightbringer', correct: false},
        { option: 'Alexandros Mograine', correct: false},
        { option: 'Sargeras The Daemon Lord', correct: false},
        { option: 'Alonsus Faol', correct: true},
      ]
    },{
      question: "In the game Subnautica a Cave Crawler will attack you.",
      category: "Entertainment Video Games",
      answer: [
        { option: 'False', correct: false},
        { option: 'All', correct: false},
        { option: 'None of These', correct: false},
        { option: 'True', correct: true},
      ]
    },{
      question: "What is the name of the device that allows for infinite energy in the anime Dimension W?",
      category: "Entertainment Japanese Anime  Manga",
      answer: [
        { option: 'Wires', correct: false},
        { option: 'Collectors', correct: false},
        { option: 'Tesla', correct: false},
        { option: 'Coils', correct: true},
      ]
    },{
      question: "What is the name of Cream the Rabbits mom in the Sonic the Hedgehog series?",
      category: "Entertainment Video Games",
      answer: [
        { option: 'Peach', correct: false},
        { option: 'Mint', correct: false},
        { option: 'Strawberry', correct: false},
        { option: 'Vanilla', correct: true},
      ]
    },{
      question: "These two countries held a commonwealth from the 16th to 18th century.",
      category: "History",
      answer: [
        { option: 'Hutu and Rwanda', correct: false},
        { option: 'North Korea and South Korea', correct: false},
        { option: 'Bangladesh and Bhutan', correct: false},
        { option: 'Poland and Lithuania', correct: true},
      ]
    },{
      question: "Which of these voices wasnt a choice for the House AI in The Simpsons Treehouse of Horror short House of Whacks?",
      category: "Entertainment Television",
      answer: [
        { option: 'Matthew Perry', correct: false},
        { option: 'Dennis Miller', correct: false},
        { option: 'Pierce Brosnan', correct: false},
        { option: 'George Clooney', correct: true},
      ]
    },{
      question: "From which album is the Gorillaz song On Melancholy Hill featured in?",
      category: "Entertainment Music",
      answer: [
        { option: 'Demon Days', correct: false},
        { option: 'Humanz', correct: false},
        { option: 'The Fall', correct: false},
        { option: 'Plastic Beach', correct: true},
      ]
    },{
      question: "Scotland voted to become an independent country during the referendum from September 2014.",
      category: "General Knowledge",
      answer: [
        { option: 'True', correct: false},
        { option: 'All', correct: false},
        { option: 'None of These', correct: false},
        { option: 'False', correct: true},
      ]
    },{
      question: "In Left 4 Dead which campaign has the protagonists going through a subway in order to reach a hospital for evacuation?",
      category: "Entertainment Video Games",
      answer: [
        { option: 'Subway Sprint', correct: false},
        { option: 'Hospital Havoc', correct: false},
        { option: 'Blood Harvest', correct: false},
        { option: 'No Mercy', correct: true},
      ]
    },{
      question: "What was the last colony the UK ceded marking the end of the British Empire?",
      category: "History",
      answer: [
        { option: 'India', correct: false},
        { option: 'Australia', correct: false},
        { option: 'Ireland', correct: false},
        { option: 'Hong Kong', correct: true},
      ]
    },
  ]

  constructor() { }

getQuizzes(){
  return this.quizzes;
}

  // constructor(private http: HttpClient) { }

  // getQuiz(): Observable<Object> {
  //   return this.http.get('https://opentdb.com/api.php?amount=10&category=18&type=multiple')
  // }
}
