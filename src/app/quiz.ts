export class Quiz {
  question: string;
  category: string;
  answer: {option: string, correct: boolean}[];
}
